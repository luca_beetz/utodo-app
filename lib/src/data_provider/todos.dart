import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:utodo_app/src/models/todo.dart';

class TodosProvider {
  final String ipAddress;

  TodosProvider({this.ipAddress});

  Future<http.Response> getAllTodos() {
    return http.get('http://${ipAddress}:8000/todos');
  }

  Future<http.Response> getTodoById(int id) {
    return http.get('http://${ipAddress}:8000/todo/${id}');
  }

  Future<http.Response> createTodo(Todo todo) {
    return http.post('http://${ipAddress}:8000/todo', body: jsonEncode(todo));
  }

  Future<http.Response> updateTodo(int id, Todo todo) {
    return http.put('http://${ipAddress}:8000/todo/${id}', body: jsonEncode(todo));
  }

  Future<http.Response> deleteTodo(int id) {
    return http.delete('http://${ipAddress}:8000/todo/${id}');
  }
}