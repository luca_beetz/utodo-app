import 'package:equatable/equatable.dart';

class Todo extends Equatable {
  final int id;
  final String title;
  final String content;

  Todo({this.id, this.title, this.content}) : super([id, title, content]);

  Todo.fromJson(Map<String, dynamic> json)
    : id = json['id'],
      title = json['title'],
      content = json['content'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'content': content,
  };
}