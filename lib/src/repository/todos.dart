import 'dart:convert';

import 'package:utodo_app/src/models/todo.dart';
import 'package:utodo_app/src/data_provider/todos.dart';

class TodosRepository {
  TodosProvider todosProvider = TodosProvider();

  List<Todo> getAllTodos() async {
    var response = await todosProvider.getAllTodos();
    List<Todo> todos = (json.decode(response.body) as List).map((i) => Todo.fromJson(i)).toList();
    return todos;
  }

  Todo getTodoById(int id) async {
    var response = await todosProvider.getTodoById(id);
    Todo todo = Todo.fromJson(json.decode(response.body));
    return todo;
  }

}