
class TodosBloc extends Bloc<TodosEvent, TodosState> {
  final TodosRepository todosRepository;

  TodosBloc({@required this.todosRepository});

  @override
  TodosState get initialState => TodosLoading();

  @override
  Stream<TodosState> mapEventToState(TodosEvent event) async* {
    if (event is LoadTodos) {
      yield* _mapLoadTodosToState();
    } else if (event is AddTodo) {
      yield* _mapAddTodoToState();
    } else if (event is UpdateTodo) {
      yield* _mapUpdateTodoToState();
    } else if (event is DeleteTodo) {
      yield* _mapDeleteTodoToState();
    }
  }

  Stream<TodosState> _mapLoadTodosToState() async* {
    
  }
}